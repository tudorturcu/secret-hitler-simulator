import random

# number of simulations (runs, games)
SIMULATION_GAME_COUNT = 10000

# number of rounds played in each run
#     we are only interested in the first 4 rounds, when
#     little information is available
ROUND_COUNT = 4

# 7 people-game parameters
PLAYER_COUNT = 7
ROLE_CARDS = (["fascist"] * 3) + (["liberal"] * 4)
POLICIES = (["liberalPolicy"] * 6) + (["fascistPolicy"] * 11)

# for both strategies, everyone is stealthy, which means:
# choosePolicyStealthy(): president removes card opposite her/his role, chancellor stays stealthy == chooses fascist policy if herself/himself is fascist and president also fascist (if fascist policy available)
def choosePolicyStealthy():
    presidentSees = []
    for var in range(3):
        presidentSees.append(policiesOrdered.pop(random.randint(0, len(policiesOrdered) - 1)))  # same as role distribution

    print "\nPresident sees: ", presidentSees
    print "President is: ", players[president]

    if players[president] == "fascist":
        if "liberalPolicy" in presidentSees:
            presidentRemoves = "liberalPolicy"
        else:
            presidentRemoves = "fascistPolicy"
    else:	# players[president] == "liberal":
        if "fascistPolicy" in presidentSees:
            presidentRemoves = "fascistPolicy"
        else:
            presidentRemoves = "liberalPolicy"

    presidentSees.remove(presidentRemoves)
    discardPile.append(presidentRemoves)

    chancellorSees = presidentSees[:]

    #print "\nChancellor sees: ", chancellorSees
    print "Chancellor is: ", players[chancellor]

    chosenCard = ""

    if players[chancellor] == "liberal":
        if "liberalPolicy" in chancellorSees:
            chosenCard = "liberalPolicy"
        else:
            chosenCard = "fascistPolicy"
    else:	# players[chancellor] == "fascist":
        if players[president] == "fascist":   # pot colabora
            if "fascistPolicy" in chancellorSees:
                chosenCard = "fascistPolicy"
            else:
                chosenCard = "liberalPolicy"
        else: #players[president] == "liberal":   # STEALTH REQUIRED
            if "liberalPolicy" in chancellorSees:
                chosenCard = "liberalPolicy"
            else:
                chosenCard = "fascistPolicy"

    # # compute what card was added to discard pile
    # chancellorSees.remove(chosenCard)

    # # add to discard pile what card remains
    # discardPile.append(chancellorSees[0])

    # print "\nDiscard pile: ", discardPile

    print "The chosen policy: ", chosenCard

    return chosenCard


# STRATEGY A: stupid (ALLEGEDLY!!!) nomination rule: chancellor is the player to the left of the president
# returns which player (represented by a number in the players[] array) will be chancellor
def strategyA ():
    return president + 1



#STRATEGY B (mirror a.k.a. equal distribution of power): 0-6, 1-5, 2-4, 3-0
# returns which player (represented by a number in the players[] array) will be chancellor
#Details:
#	first president chooses as current chancellor the person to her/his right
#	second president (left of the previous president)  chooses as current chancellor the person to the last chancellor's right
#	third president (left of the previous president) chooses as current chancellor the person to the last chancellor's right
#	fourth president (left of the previous president) chooses as current chancellor the first president
#	(probably still not optimal, but probably better than strategy A)

def strategyB ():
    if president == 3:
        return 0
    return 6 - president


############################ MAIN START ############################

totalLiberalPoliciesPassed = 0.0
totalFascistPoliciesPassed = 0.0

histogramLiberalPoliciesPassed = [0] * (ROUND_COUNT + 1)

# Start simulations
for runNo in range(SIMULATION_GAME_COUNT):
    discardPile = []

    # MUST select random from them (like selecting the role)
    policiesOrdered = list(POLICIES)
    # print "\nPolicies (6 liberal, 11 fascist): ", policiesOrdered

    # start current run
    print "\n\nRun NO: ", runNo

    # random ROLE_CARDS distributed to players
    # for 7 players: 4 liberal, 3 fascist
    # each player will receive a role card from the ROLE_CARDS array
    players = list(ROLE_CARDS)
    random.shuffle(players)
    print "\nPlayers: ", players

    # presidency starts from the first player ...
    president = 0

    # number of policies passed this run
    liberalPoliciesPassed = 0
    fascistPoliciesPassed = 0

    for rounds in range(ROUND_COUNT):
        # Choose chancellor

        # chancellor = strategyA()
        chancellor = strategyB()

        # print president, chancellor

        if choosePolicyStealthy() == "liberalPolicy":
            liberalPoliciesPassed += 1
        else:
            fascistPoliciesPassed += 1

        print "\nLiberal: ", liberalPoliciesPassed, "; Fascist: ", fascistPoliciesPassed

        president += 1
        # ... and presidency advances clockwise

    totalLiberalPoliciesPassed += liberalPoliciesPassed
    totalFascistPoliciesPassed += fascistPoliciesPassed

    histogramLiberalPoliciesPassed[liberalPoliciesPassed] += 1


print "\n\n\nAverage Liberal: ", totalLiberalPoliciesPassed / SIMULATION_GAME_COUNT, "; Average Fascist: ", totalFascistPoliciesPassed / SIMULATION_GAME_COUNT
print histogramLiberalPoliciesPassed
